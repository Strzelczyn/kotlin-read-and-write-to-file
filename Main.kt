import java.io.File

fun main(args: Array<String>) {
    var valuesReaded: List<String> = readFromFile()
    var average: Double = getAverage(valuesReaded)
    writeToFile(average)
}

fun readFromFile(): List<String> {
    var path: String = "E:fileToRead.txt"
    var myFile = File(path)
    return myFile.readLines()
}

fun getAverage(items: List<String>): Double {
    var numberOfItems: Int = items.size
    var itemsValues: Double = 0.0
    items.forEach {
        itemsValues += it.toDouble()
    }
    return itemsValues / numberOfItems
}

fun writeToFile(value: Double) {
    var path: String = "E:fileToWrite.txt"
    var myFile = File(path)
    myFile.writeText(value.toString())
}